terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      #version = "3.74.0"
    }
  }
}

data "terraform_remote_state" "terstate" {
  backend = "gcs"
  config = {
    credentials = "epamgcp-c485436c84c7.json"
    bucket      = "epamter"
    prefix      = "terraform/state"
  }
}

#resource "template_file" "terst" {
#  template = greeting
#
#  vars {
#    greeting = data.terraform_remote_state.terstate.greeting
#  }
#}

#terraform {
#  backend "gcs" {
#    credentials = "epamgcp-c485436c84c7.json"
#	project     = "epamgcp"
#	bucket      = "epamter"
#    prefix      = "terraform/state"
#    
#  }
#}

provider "google" {
  credentials = file("epamgcp-c485436c84c7.json")
  project     = "epamgcp"
  region      = "asia-east2"
  zone        = "asia-east2-a"
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}

#module "cloud-nat" {
#  source     = "terraform-google-modules/cloud-nat/google"
#  version    = "~> 1.2"
#  project_id = epamgcp
#  region     = asia-east2
#  router     = google_compute_router.router.name
#  nat_ip_allocate_option = "MANUAL_ONLY"
#}

resource "google_sql_database" "database" {
  name     = "my-database"
  instance = google_sql_database_instance.instance.name
}

resource "google_sql_database_instance" "instance" {
  name   = "my-database-instance-2"
  region = "asia-east2"
  settings {
    tier = "db-f1-micro"
  }

  deletion_protection = "false"
}

resource "google_storage_bucket" "epambucket" {
  name          = "image-store.com"
  location      = "EU"
  force_destroy = true

  uniform_bucket_level_access = true

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
  cors {
    origin          = ["http://image-store.com"]
    method          = ["GET", "HEAD", "PUT", "POST", "DELETE"]
    response_header = ["*"]
    max_age_seconds = 3600
  }
}

resource "google_service_account" "epam-sa" {
  account_id   = "epam-sa"
  display_name = "SA"
}

resource "google_project_iam_member" "epam-sa" {
  # project = epamgcp
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${google_service_account.epam-sa.email}"
}

resource "google_compute_instance" "default" {
  name         = "epamci"
  machine_type = "f1-micro"
  zone         = "asia-east2-a"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {}
  }

  metadata_startup_script = <<EOF
#!/bin/bash\
apt update -y
apt install git -y
apt install software-properties-common -y
add-apt-repository --yes --update ppa:ansible/ansible
apt install ansible -y
EOF

  service_account {
    email  = google_service_account.epam-sa.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_instance_group" "epam-cig" {
  name    = "terraform-cig"
  zone    = "asia-east2-a"
  network = google_compute_network.default.id
}

resource "google_compute_firewall" "default" {
  name    = "epam-firewall"
  network = google_compute_network.default.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }

  source_tags = ["web"]
}

resource "google_compute_network" "default" {
  name = "epam-network"
}

#module "lb-http" {
 # source  = "GoogleCloudPlatform/lb-http/google//modules/serverless_negs"
 # version = "~> 4.4"
#
 # project = "epamgcp"
#  name    = "my-lb"
#
 # ssl                             = true
 # managed_ssl_certificate_domains = ["your-domain.com"]
 # https_redirect                  = true
 # backends = {
 #   default = {
  #    description            = null
   #   enable_cdn             = false
    #  custom_request_headers = null
     # security_policy        = null
#
#
 #     log_config = {
  #      enable      = true
   #     sample_rate = 1.0
    #  }
#
 #     groups = [
  #      {
   #       # Your serverless service should have a NEG created that's referenced here.
    #      group = var.backend
	#	  balancing_mode               = null
     #     capacity_scaler              = null
      #    description                  = null
       #   max_connections              = null
        #  max_connections_per_instance = null
        #  max_connections_per_endpoint = null
      #    max_rate                     = null
       #   max_rate_per_instance        = null
       #   max_rate_per_endpoint        = null
       #   max_utilization              = null
       # }
     # ]
#
 #     iap_config = {
  #      enable               = false
   #     oauth2_client_id     = null
    #    oauth2_client_secret = null
   #   }
   # }
 # }
#}

